# Nettoyages mots-clés SP

## Méthode

À partir de l'archive sp-all (fait avec le script update_articles.sh qui merge SP-articles et SP-archives) on ouvre une base avec basez.
En Xpath:

```
//div[@class='keywords']//span[@class='label']/text()
```

Cela donne la liste de tous les mots utilisés comme mots-clés éditeurs dans l'ensemble du corpus SP

Ensuite on les mets dans un fichier texte et on les classe en ordre alphabétique en éliminant les doublons:

```
sort -u nomfichier.txt
```

Cela donne le fichier keywordList.txt

À partir de ce fichier on vérifie quels mots sont dans les listes spip-mots.csv et groupes.xml avec le script scriptmatch.sh

Cela donne une liste que l'on peu ensuite ordonner en ordre alphabétique avec `sort -u`

